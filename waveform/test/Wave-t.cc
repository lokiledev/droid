#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "waveform/Wave.h"

using ::testing::Return;
using ::testing::_;

class WaveMock : public Wave
{
public:
    MOCK_METHOD(Command, compute, (double), (override));
};

TEST(WaveTest, enable_disable)
{
    WaveMock wave;

    // Object is disabled, don't compute output.
    EXPECT_CALL(wave, compute(_))
        .Times(0);

    ASSERT_EQ(0.0, wave.update().position);
    ASSERT_FALSE(wave.enabled());

    EXPECT_CALL(wave, compute)
        .WillOnce(Return(Command{10.0}));
    wave.start();
    EXPECT_EQ(10.0, wave.update().position);
    ASSERT_TRUE(wave.enabled());
}


TEST(WaveTest, clock_lambda)
{
    double time{0.0};
    bool called{false};

    WaveMock wave;

    // Pass the lambda as the clock function.
    wave.set_clock([&]() {called = true; return time;});

    // Clock is called once to store start time (0 here)
    wave.start();
    ASSERT_TRUE(called);
    called = false;

    time = 10.0; // change the mocked time.
    // Compute shall be called with the new delta time 10 - 0;
    EXPECT_CALL(wave, compute(time))
        .WillOnce(Return(Command{1.0}));

    ASSERT_EQ(1.0, wave.update().position);
    ASSERT_TRUE(called);
}
