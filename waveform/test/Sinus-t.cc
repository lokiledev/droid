#include <gtest/gtest.h>
#include <cmath>

#include "waveform/Sinus.h"

double const TOLERANCE{2.0 * std::numeric_limits<double>::epsilon()};
TEST(Sinus, constructors)
{
    Sinus s;
    Sinus s1{1.0};
    Sinus s2{1.0, 10.0};
    Sinus s3{1.0, 10.0, M_PI};
    Sinus s4{1.0, 10.0, M_PI, 1.0};
}

TEST(Sinus, default_values)
{
    Sinus s;
    ASSERT_DOUBLE_EQ(0.0, s.compute(0.0).position);
    ASSERT_NEAR(1.0, s.compute(0.0).velocity, TOLERANCE);

    ASSERT_NEAR(1.0, s.compute(0.25).position, TOLERANCE);
    ASSERT_NEAR(0.0, s.compute(0.25).velocity, TOLERANCE);

    ASSERT_NEAR(0.0, s.compute(0.5).position, TOLERANCE);
    ASSERT_NEAR(-1.0, s.compute(0.5).velocity, TOLERANCE);

    ASSERT_NEAR(-1.0, s.compute(0.75).position, TOLERANCE);
    ASSERT_NEAR(0.0, s.compute(0.75).velocity, TOLERANCE);

    ASSERT_NEAR(0.0, s.compute(1.0).position, TOLERANCE);
    ASSERT_NEAR(1.0, s.compute(1.0).velocity, TOLERANCE);
}
