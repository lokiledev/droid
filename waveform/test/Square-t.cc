#include <gtest/gtest.h>
#include <cmath>

#include "waveform/Square.h"

double const TOLERANCE{2.0 * std::numeric_limits<double>::epsilon()};

TEST(Square, constructors)
{
    Square s;
    Square s1{1.0};
    Square s2{1.0, 10.0};
    Square s3{1.0, 10.0, 10.0};
}

TEST(Square, default_values)
{
    Square s;
    ASSERT_DOUBLE_EQ(1, s.compute(0.0).position);
    ASSERT_DOUBLE_EQ(0.0, s.compute(0.0).velocity);

    ASSERT_NEAR(1.0, s.compute(0.5).position, TOLERANCE);
    ASSERT_DOUBLE_EQ(0.0, s.compute(0.5).velocity);

    ASSERT_NEAR(-1.0, s.compute(0.51).position, TOLERANCE);
    ASSERT_DOUBLE_EQ(0.0, s.compute(0.51).velocity);

    ASSERT_NEAR(-1.0, s.compute(0.99).position, TOLERANCE);
    ASSERT_DOUBLE_EQ(0.0, s.compute(0.99).velocity);

    ASSERT_NEAR(1.0, s.compute(1.0).position, TOLERANCE);
    ASSERT_DOUBLE_EQ(0.0, s.compute(1.0).velocity);
}
