#include <gtest/gtest.h>
#include "waveform/Ramp.h"

TEST(Ramp, compute_no_offset)
{
    Ramp ramp(1.0, 0.0);
    ASSERT_DOUBLE_EQ(0.0, ramp.compute(0.0).position);
    ASSERT_DOUBLE_EQ(1.0, ramp.compute(0.0).velocity);

    ASSERT_DOUBLE_EQ(1.0, ramp.compute(1.0).position);
    ASSERT_DOUBLE_EQ(1.0, ramp.compute(0.0).velocity);

    ASSERT_DOUBLE_EQ(10.0, ramp.compute(10.0).position);
    ASSERT_DOUBLE_EQ(1.0, ramp.compute(0.0).velocity);
}

TEST(Ramp, compute_with_offset)
{
    Ramp ramp(2.0, 1.0); // 2x +1
    ASSERT_DOUBLE_EQ(1.0, ramp.compute(0.0).position);
    ASSERT_DOUBLE_EQ(2.0, ramp.compute(0.0).velocity);

    ASSERT_DOUBLE_EQ(3.0, ramp.compute(1.0).position);
    ASSERT_DOUBLE_EQ(2.0, ramp.compute(1.0).velocity);

    ASSERT_DOUBLE_EQ(21.0, ramp.compute(10.0).position);
    ASSERT_DOUBLE_EQ(2.0, ramp.compute(10.0).velocity);
}
