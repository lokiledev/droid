#include <gtest/gtest.h>
#include <cmath>

#include "waveform/Triangle.h"

double const TOLERANCE{2.0 * std::numeric_limits<double>::epsilon()};

TEST(Triangle, constructors)
{
    Triangle t;
    Triangle t1{1.0};
    Triangle t2{1.0, 10.0};
    Triangle t3{1.0, 10.0, 10.0};
}

TEST(Triangle, default_values)
{
    Triangle t;
    ASSERT_DOUBLE_EQ(-1.0, t.compute(0.0).position);
    ASSERT_DOUBLE_EQ(1.0, t.compute(0.0).velocity);

    ASSERT_DOUBLE_EQ(1.0, t.compute(0.25).velocity);
    ASSERT_NEAR(0.0, t.compute(0.25).position, TOLERANCE);

    // Change of velocity sign at peak of the triangle
    ASSERT_DOUBLE_EQ(1.0, t.compute(0.4).velocity);
    ASSERT_NEAR(1.0, t.compute(0.5).position, TOLERANCE);
    ASSERT_DOUBLE_EQ(-1.0, t.compute(0.6).velocity);

    ASSERT_NEAR(0.0, t.compute(0.75).position, TOLERANCE);

    ASSERT_DOUBLE_EQ(-1.0, t.compute(0.99).velocity);
    ASSERT_NEAR(-1.0, t.compute(1.0).position, TOLERANCE);
    ASSERT_DOUBLE_EQ(1.0, t.compute(1.01).velocity);
}
