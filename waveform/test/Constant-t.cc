#include <gtest/gtest.h>
#include "waveform/Constant.h"

TEST(Constant, default_constructor)
{
    Constant cst();
}

TEST(Constant, initialization_list)
{
    Constant cst{10.0};
}

TEST(Constant, update)
{
    Constant cst{10.0};
    cst.start();
    Command const result = cst.update();
    ASSERT_DOUBLE_EQ(10.0, result.position);
    ASSERT_DOUBLE_EQ(0.0, result.velocity);
}

TEST(Constant, compute)
{
    Constant cst{10.0};
    cst.start();
    // Ouput should not depend on time
    ASSERT_DOUBLE_EQ(10.0, cst.compute(100.0).position);
    ASSERT_DOUBLE_EQ(0.0, cst.compute(100.0).velocity);

    // Ouput should not depend on time
    ASSERT_DOUBLE_EQ(10.0, cst.compute(200.0).position);
    ASSERT_DOUBLE_EQ(0.0, cst.compute(200.0).velocity);
}
