#include <gtest/gtest.h>
#include "waveform/Step.h"

TEST(Step, compute)
{
    Step step(1.0, 10.0);
    ASSERT_DOUBLE_EQ(0.0, step.compute(0.0).position);
    ASSERT_DOUBLE_EQ(0.0, step.compute(0.0).velocity);

    ASSERT_DOUBLE_EQ(0.0, step.compute(9.0).position);
    ASSERT_DOUBLE_EQ(0.0, step.compute(9.0).velocity);

    ASSERT_DOUBLE_EQ(1.0, step.compute(10.0).position);
    ASSERT_DOUBLE_EQ(0.0, step.compute(10.0).velocity);

    ASSERT_DOUBLE_EQ(1.0, step.compute(20.0).position);
    ASSERT_DOUBLE_EQ(0.0, step.compute(20.0).velocity);
}
