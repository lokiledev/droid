
# add_executable(wavegen_unit
#     ${CMAKE_CURRENT_SOURCE_DIR}/unit/Wave-t.cc
#     ${CMAKE_CURRENT_SOURCE_DIR}/unit/Constant-t.cc
#     ${CMAKE_CURRENT_SOURCE_DIR}/unit/Ramp-t.cc
#     ${CMAKE_CURRENT_SOURCE_DIR}/unit/Step-t.cc
#     ${CMAKE_CURRENT_SOURCE_DIR}/unit/Sinus-t.cc
# )
# target_link_libraries(wavegen_unit wavegen GTest::GTest GTest::Main gmock)
# gtest_discover_tests(wavegen_unit)

cmake_minimum_required(VERSION 3.16)
project(waveform)

# Default to C99
if(NOT CMAKE_C_STANDARD)
  set(CMAKE_C_STANDARD 99)
endif()

# Default to C++14
if(NOT CMAKE_CXX_STANDARD)
  set(CMAKE_CXX_STANDARD 14)
endif()

if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
  add_compile_options(-Wall -Wextra -Wpedantic)
endif()

# find dependencies
find_package(ament_cmake REQUIRED)

add_library(waveform STATIC
    ${CMAKE_CURRENT_SOURCE_DIR}/src/Clock.cc
    ${CMAKE_CURRENT_SOURCE_DIR}/src/Wave.cc
    ${CMAKE_CURRENT_SOURCE_DIR}/src/Constant.cc
    ${CMAKE_CURRENT_SOURCE_DIR}/src/Ramp.cc
    ${CMAKE_CURRENT_SOURCE_DIR}/src/Step.cc
    ${CMAKE_CURRENT_SOURCE_DIR}/src/Square.cc
    ${CMAKE_CURRENT_SOURCE_DIR}/src/Triangle.cc
    ${CMAKE_CURRENT_SOURCE_DIR}/src/Sinus.cc)

target_include_directories(waveform PUBLIC
  "$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>"
  "$<INSTALL_INTERFACE:include>")
  
# Based on https://index.ros.org/doc/ros2/Tutorials/Ament-CMake-Documentation/
# This shouldn't be necessary because of the `target_include_directories`
# However I can't make it build in one shot and find the includes without this macro.
ament_export_include_directories(include)

# Export target so that dependents can link with this lib using
# target_link_libraries(my_target pid::pid)
ament_export_targets(export_waveform)

install(
  DIRECTORY include/
  DESTINATION include)

install(
  TARGETS waveform
  EXPORT export_waveform
  LIBRARY DESTINATION lib
  ARCHIVE DESTINATION lib
  RUNTIME DESTINATION bin
  INCLUDES DESTINATION include
)

if(BUILD_TESTING)
  find_package(ament_lint_auto REQUIRED)
  # the following line skips the linter which checks for copyrights
  # uncomment the line when a copyright and license is not present in all source files
  #set(ament_cmake_copyright_FOUND TRUE)
  # the following line skips cpplint (only works in a git repo)
  # uncomment the line when this package is not in a git repo
  #set(ament_cmake_cpplint_FOUND TRUE)
  ament_lint_auto_find_test_dependencies()

  ament_add_gmock(${PROJECT_NAME}_test
    ${CMAKE_CURRENT_SOURCE_DIR}/test/Wave-t.cc
    ${CMAKE_CURRENT_SOURCE_DIR}/test/Constant-t.cc
    ${CMAKE_CURRENT_SOURCE_DIR}/test/Ramp-t.cc
    ${CMAKE_CURRENT_SOURCE_DIR}/test/Step-t.cc
    ${CMAKE_CURRENT_SOURCE_DIR}/test/Square-t.cc
    ${CMAKE_CURRENT_SOURCE_DIR}/test/Triangle-t.cc
    ${CMAKE_CURRENT_SOURCE_DIR}/test/Sinus-t.cc)
    target_link_libraries(${PROJECT_NAME}_test waveform)
endif()

ament_package()
