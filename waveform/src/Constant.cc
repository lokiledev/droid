#include "waveform/Constant.h"

Constant::Constant(double value)
    : k_{value}
{
}

Command Constant::compute(double time)
{
    (void) time; // unused for a constant
    return Command{k_, 0.0};
}
