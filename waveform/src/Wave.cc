#include "waveform/Wave.h"

Command Wave::update() {
    if (is_enabled_) {
        double t = clock_() - start_;
        return compute(t);
    }
    return Command{};
}
