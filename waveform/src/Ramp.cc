#include "waveform/Ramp.h"

Command Ramp::compute(double time)
{
    double pos = a_ * time + b_;
    double vel = a_;
    return Command{pos, vel};
}