#include <cmath>

#include "waveform/Square.h"

Command Square::compute(double time)
{
    double t = fmod(time, t_);
    if (t <= t_/2.0)
    {
        return Command{a_ + k_};
    }

    return Command{-a_ + k_};
}