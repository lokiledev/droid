#include "waveform/Clock.h"

double now()
{
    auto now = std::chrono::steady_clock::now();
    std::chrono::nanoseconds elapsed = now.time_since_epoch();
    return std::chrono::duration_cast<second>(elapsed).count();
}
