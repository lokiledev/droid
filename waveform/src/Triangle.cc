#include <cmath>

#include "waveform/Triangle.h"

Command Triangle::compute(double time)
{
    double t = fmod(time, t_);
    Command c;
    if (t <= t_/2.0)
    {
        c.position = -a_/2.0 + a_* 2.0 * t / t_ + k_;
        c.velocity = 2.0 * a_ / t_;
    }
    else
    {
        c.position = a_/2.0 - a_*2.0*(t / t_ - 0.5) + k_;
        c.velocity = -2.0 * a_ / t_;
    }

    return c;
}