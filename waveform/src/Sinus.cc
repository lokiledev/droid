#include <cmath>

#include "waveform/Sinus.h"

Command Sinus::compute(double time)
{
    double pos = a_ * sin( 2.0 * M_PI *f_ * time - p_) + k_;
    double vel = a_ * cos( 2.0 * M_PI *f_ * time - p_);
    return Command{pos, vel};
}