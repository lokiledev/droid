#include "waveform/Step.h"

Command Step::compute(double time)
{
    if (time < t0_)
    {
        return Command{};
    }
    return Command{a_};
}
