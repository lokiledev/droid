#ifndef WAVEFORM_SQUARE_H
#define WAVEFORM_SQUARE_H

#include "Wave.h"

/// \class Square : A Square Wave generator
class Square : public Wave
{
public:
  Square(double frequency = 1.0,
        double amplitude = 1.0,
        double offset = 0.0)
    : t_{1.0 / frequency}, a_{amplitude}, k_{offset} {}

  ~Square() = default;

  /// \brief Compute the output of the square at current time.
  /// \details equation is y = A + offset if t < T/2, -A + offset else;
  ///          at edges velocity shall be infinite but here it is kept at 0.
  Command compute(double time) override;
private:
   double t_{1.0}; ///< The period of the square in s.
   double a_{1.0}; ///< The amplitude of the square.
   double k_{0.0}; ///< Offset
};
#endif