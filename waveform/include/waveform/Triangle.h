#ifndef WAVEFORM_TRIANGLE_H
#define WAVEFORM_TRIANGLE_H

#include "Wave.h"

/// \class Triangle : A Triangle Wave generator
class Triangle : public Wave
{
public:
  Triangle(double frequency = 1.0,
        double amplitude = 1.0,
        double offset = 0.0)
    : t_{1.0/frequency}
    , a_{amplitude}
    , k_{offset} {}

  ~Triangle() = default;

  /// \brief Compute the output of the triangle at current time.
  /// \details It is centered on 0 so it goes between -A/2 and +A/2.
  /// \see https://fr.wikipedia.org/wiki/Signal_triangulaire for equation.
  Command compute(double time) override;
private:
   double t_{1.0}; ///< The period of the triangle in s.
   double a_{1.0}; ///< The amplitude of the triangle.
   double k_{0.0}; ///< Offset
};
#endif