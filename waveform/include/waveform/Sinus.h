#ifndef WAVEFORM_SINUS_H
#define WAVEFORM_SINUS_H

#include "Wave.h"

/// \class Sinus : A Sinus Wave generator
class Sinus : public Wave
{
public:
  Sinus(double frequency = 1.0,
        double amplitude = 1.0,
        double phase = 0.0,
        double offset = 0.0)
    : f_{frequency}, a_{amplitude}, p_{phase}, k_{offset} {}

  ~Sinus() = default;

  /// \brief Compute the output of the sinus at current time.
  /// \details equation is y = A*sin(2*pi*f*t - phase) + k;
  Command compute(double time) override;
private:
   double f_{1.0}; ///< The frequency of the sinus in Hz..
   double a_{1.0}; ///< The amplitude of the sinus.
   double p_{0.0}; ///< Phase in range [-PI, PI]
   double k_{0.0}; ///< Offset
};
#endif