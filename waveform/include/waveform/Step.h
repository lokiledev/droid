#ifndef WAVEFORM_STEP_H
#define WAVEFORM_STEP_H

#include "Wave.h"

/// \class Step: A Step generator
class Step : public Wave
{
public:
  Step(double value, double delay)
    : a_{value}, t0_{delay} {}
  ~Step() = default;

  /// \brief Compute the output of the step at current time.
  /// \details equation is y = 0 if t0 < delay, a else;
  Command compute(double time) override;
private:
   double a_{0.0}; ///< The amplitude of the step.
   double t0_{0.0}; ///< The offset.
};
#endif