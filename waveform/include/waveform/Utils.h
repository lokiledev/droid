#ifndef WAVEFORM_UTILS_H
#define WAVEFORM_UTILS_H

#include <cmath>

constexpr double rad2deg(double const radians) 
{
    return radians * (180.0/M_PI);
};

constexpr double deg2rad(double const degrees) 
{
    return degrees * (M_PI / 180.0);
};

#endif