#ifndef WAVEFORM_CONSTANT_H
#define WAVEFORM_CONSTANT_H

#include "Wave.h"

/// \class Constant: A constant output generator.
class Constant : public Wave
{
public:
  Constant() = default;
  Constant(double value);
  ~Constant() = default;

  /// \brief Compute the output (constant value here).
  /// \details equation is y = K;
  Command compute(double time) override;
private:
   double k_{0.0}; ///< The constant value.
};
#endif