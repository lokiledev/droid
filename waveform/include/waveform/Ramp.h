#ifndef WAVEFORM_RAMP_H
#define WAVEFORM_RAMP_H

#include "Wave.h"

/// \class Ramp: A Ramp generator
class Ramp : public Wave
{
public:
  Ramp(double gain, double offset)
    : a_{gain}, b_{offset} {}

  ~Ramp() = default;

  /// \brief Compute the output (constant value here).
  /// \details equation is y = a*t + b;
  Command compute(double time) override;
private:
   double a_{0.0}; ///< The the gain of the slope.
   double b_{0.0}; ///< The offset.
};
#endif