#ifndef WAVEFORM_CLOCK_H
#define WAVEFORM_CLOCK_H

#include <functional>
#include <chrono>

/// \brief Seconds unit with double precision
/// \details can be converted to/from integer based durations using duration_cast.
using second = std::chrono::duration<double, std::ratio<1>>;

/// \brief A common type for Clock functions
using Clock = std::function<double ()>;

/// \brief A default clock giving absolute current time since epoch in seconds.
double now();

#endif