#ifndef WAVEFORM_WAVE_H
#define WAVEFORM_WAVE_H
#include <cstdint>

#include "Clock.h"

/// \struct Command the command output of a wave signal
/// \brief The velocity is the derivative of the position.
struct Command {
  double position{0.0};
  double velocity{0.0};
};

/// \class Wave: An abstract wave pattern generator
/// \details Create periodic signals in function of some time reference.

class Wave {
public:
  Wave() = default;
  void set_clock(Clock clock) { clock_ = clock; };
  virtual ~Wave() = default;

  /// \brief update the output.
  /// \return The value of the output at current clock time.
  Command update();

  void start() { is_enabled_ = true; start_ = clock_();}
  void stop() { is_enabled_ = false; }
  bool enabled() const { return is_enabled_;}
protected:
  /// \brief The method to be implemented by each daughter class
  ///        to compute the output based on the type of pattern and clock.
  /// \param[in] time : double - The time to pass to the pattern out = f(t).
  virtual Command compute(double time) = 0;

  Clock clock_{now}; ///< A clock function giving current time in seconds.
  double period_{-1.0}; ///< Period of the pattern in seconds.
  int n_loops_{0}; ///< Number of loops of the period, 0 == Infinite
  double start_{0.0};
  bool is_enabled_{false};
};
#endif