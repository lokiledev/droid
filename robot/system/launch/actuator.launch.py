import os
from pathlib import Path

from ament_index_python.packages import get_package_share_directory

from launch import LaunchDescription
from launch.actions import IncludeLaunchDescription
from launch.actions import DeclareLaunchArgument, TimerAction
from launch.conditions import IfCondition, UnlessCondition
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node

def generate_launch_description():
    pkg_droid = Path(get_package_share_directory('droid_description'))
    urdf_file = pkg_droid / 'urdf/actuator/robot.urdf'
    configs = Path(get_package_share_directory('system')) / 'config'

    rviz = Node(
        package='rviz2',
        executable='rviz2',
        arguments=['-d', str(configs/ 'actuator.rviz')],
        condition=IfCondition(LaunchConfiguration('rviz'))
    )

    hal = Node(
        package='hal',
        executable='droid_hal',
        name="hal",
        output='screen',
        condition=UnlessCondition(LaunchConfiguration('sim'))
    )

    with urdf_file.open('r') as urdf:
        robot_description = urdf.read()

    robot_state_publisher = Node(
        package='robot_state_publisher',
        executable='robot_state_publisher',
        name="droid_rsp",
        output='screen',
        parameters=[{"robot_description" : robot_description}])

    control_tester = Node(
        package='joint_position_controller',
        executable='control_tester',
        name="control_tester",
        output='screen')

    return LaunchDescription([
        DeclareLaunchArgument('rviz', default_value='false', description='Open RViz.'),
        hal,
        control_tester,
        robot_state_publisher,
        TimerAction(period=2.0, actions=[rviz])
    ])
