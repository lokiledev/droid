import os
from pathlib import Path

from ament_index_python.packages import get_package_share_directory

from launch import LaunchDescription
from launch.actions import IncludeLaunchDescription
from launch.actions import DeclareLaunchArgument, TimerAction
from launch.conditions import IfCondition, UnlessCondition
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node

def generate_launch_description():
    pkg_gazebo_ros = Path(get_package_share_directory('gazebo_ros'))
    pkg_droid = Path(get_package_share_directory('droid_description'))
    urdf_file = pkg_droid / 'actuator/actuator.urdf'
    world_file = pkg_droid / 'simulation/worlds/actuator.world'
    configs = Path(get_package_share_directory('system')) / 'config'

    sim = LaunchConfiguration('sim', default='true')

    # Gazebo launch
    gazebo = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(str(pkg_gazebo_ros / 'launch/gazebo.launch.py')),
        condition=IfCondition(LaunchConfiguration('sim'))
    )

    rviz = Node(
        package='rviz2',
        executable='rviz2',
        arguments=['-d', str(configs/ 'droid.rviz')],
        condition=IfCondition(LaunchConfiguration('rviz'))
    )

    hal = Node(
        package='hal',
        executable='droid_hal',
        namespace="droid",
        name="hal",
        output='screen',
        condition=UnlessCondition(LaunchConfiguration('sim'))
    )

    robot_state_publisher = Node(
        package='robot_state_publisher',
        executable='robot_state_publisher',
        namespace="droid",
        name="droid_rsp",
        output='screen',
        parameters=[{"use_sim_time" : sim}],
        arguments=[str(urdf_file)])

    control_tester = Node(
        package='joint_position_controller',
        executable='control_tester',
        namespace="droid",
        name="control_tester",
        output='screen')

    return LaunchDescription([
        DeclareLaunchArgument('sim', default_value='true', description='Run stack in simulation.'),
        DeclareLaunchArgument('world', default_value=str(world_file), description='Simulation world file'),
        DeclareLaunchArgument('rviz', default_value='false', description='Open RViz.'),
        hal,
        gazebo,
        robot_state_publisher,
        control_tester,
        TimerAction(period=5.0, actions=[rviz])
    ])
