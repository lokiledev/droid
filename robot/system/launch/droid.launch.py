import os
from pathlib import Path

from ament_index_python.packages import get_package_share_directory

from launch import LaunchDescription
from launch.actions import IncludeLaunchDescription
from launch.actions import DeclareLaunchArgument, TimerAction
from launch.conditions import IfCondition, UnlessCondition
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node

def generate_launch_description():
    pkg_gazebo_ros = Path(get_package_share_directory('gazebo_ros'))
    pkg_droid = Path(get_package_share_directory('droid_description'))
    urdf_file = pkg_droid / 'urdf/droid/robot.urdf'
    world_file = pkg_droid / 'simulation/worlds/droid.world'
    configs = Path(get_package_share_directory('system')) / 'config'

    sim = LaunchConfiguration('sim', default='false')

    # Gazebo launch
    gazebo = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(str(pkg_gazebo_ros / 'launch/gazebo.launch.py')),
        condition=IfCondition(LaunchConfiguration('sim'))
    )

    rviz = Node(
        package='rviz2',
        executable='rviz2',
        arguments=['-d', str(configs/ 'droid.rviz')],
        condition=IfCondition(LaunchConfiguration('rviz'))
    )

    with urdf_file.open('r') as urdf:
        robot_description = urdf.read()

    joint_state_publisher = Node(
        package='joint_state_publisher',
        executable='joint_state_publisher',
        name="droid_jsp",
        output='screen',
        arguments=[str(urdf_file)],
        condition=UnlessCondition(LaunchConfiguration('sim'))
    )

    joint_state_publisher_gui = Node(
        package='joint_state_publisher_gui',
        executable='joint_state_publisher_gui',
        name="droid_jsp_gui",
        output='screen',
        arguments=[str(urdf_file)],
        condition=UnlessCondition(LaunchConfiguration('sim'))
    )

    robot_state_publisher = Node(
        package='robot_state_publisher',
        executable='robot_state_publisher',
        name="droid_rsp",
        output='screen',
        parameters=[{"robot_description" : robot_description, "use_sim_time" : sim}])

    return LaunchDescription([
        DeclareLaunchArgument('sim', default_value='false', description='Run stack in simulation.'),
        DeclareLaunchArgument('world', default_value=str(world_file), description='Simulation world file'),
        DeclareLaunchArgument('rviz', default_value='false', description='Open RViz.'),
        joint_state_publisher,
        joint_state_publisher_gui,
        robot_state_publisher,
        gazebo,
        TimerAction(period=2.0, actions=[rviz])
    ])
