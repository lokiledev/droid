import os
from pathlib import Path

from ament_index_python.packages import get_package_share_directory

from launch import LaunchDescription
from launch.actions import IncludeLaunchDescription
from launch.actions import DeclareLaunchArgument, TimerAction
from launch.conditions import IfCondition, UnlessCondition
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node

def generate_launch_description():
    pkg_droid = Path(get_package_share_directory('droid_description'))
    urdf_file = pkg_droid / 'urdf/left_leg/robot.urdf'

    configs = Path(get_package_share_directory('system')) / 'config'
    sim = LaunchConfiguration('sim', default='false')

    hal = Node(
        package='hal',
        executable='droid_hal',
        name="hal",
        output='screen',
        condition=UnlessCondition(LaunchConfiguration('sim'))
    )

    rviz = Node(
        package='rviz2',
        executable='rviz2',
        arguments=['-d', str(configs/ 'left_leg.rviz')],
        condition=IfCondition(LaunchConfiguration('rviz'))
    )

    with urdf_file.open('r') as urdf:
        robot_description = urdf.read()

    robot_state_publisher = Node(
        package='robot_state_publisher',
        executable='robot_state_publisher',
        name="droid_rsp",
        output='screen',
        parameters=[{"robot_description" : robot_description, "use_sim_time" : sim}])

    # joint_state_publisher_gui = Node(
    #     package='joint_state_publisher_gui',
    #     executable='joint_state_publisher_gui',
    #     name="droid_jsp_gui",
    #     output='screen',
    #     arguments=[str(urdf_file)]
    #     #condition=UnlessCondition(LaunchConfiguration('sim'))
    # )

    control_tester = Node(
        package='joint_position_controller',
        executable='control_tester',
        name="control_tester",
        output='screen')

    return LaunchDescription([
        DeclareLaunchArgument('sim', default_value='false', description='Run stack in simulation.'),
        DeclareLaunchArgument('rviz', default_value='false', description='Open RViz.'),
        robot_state_publisher,
        hal,
        control_tester,
        #TimerAction(period=30.0, actions=[joint_state_publisher_gui]),
        TimerAction(period=2.0, actions=[rviz])
    ])
