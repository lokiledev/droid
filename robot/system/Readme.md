# System package

These are system wide launch files and configurations
for a robot.

## Definitions

A ROS software stack for a system is a combination
of many ROS nodes, their specific parameters, and the target
for which it is launched:
* Robot: On the real robot
* Sim: Simulated (in gazebo on the local computer)

Generally a URDF model of the robot is provided
and used by some of these nodes.

A subsystem is like a subassembly of the robot.
Sometimes when prototyping or bringing up the hardware,
we want to execute the stack for example for a single actuator.
Or a single leg of the robot.

All this is possible thanks to launch files and their parameters.

# Choosing targets

By convention, all launch files in this package have a `sim` parameter.

So you can easily choose to run the stack for the real robot or for the simulation by setting this parameter.
It is `true` by default so that you don't launch
the robot moving software by accident.