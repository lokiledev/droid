#include <gazebo/common/Events.hh>
#include <gazebo/common/Time.hh>
#include <gazebo/physics/Joint.hh>
#include <gazebo/physics/Model.hh>
#include <gazebo/physics/World.hh>

#include <joint_effort_plugin/joint_effort_plugin.hpp>

#include <gazebo_ros/conversions/builtin_interfaces.hpp>
#include <gazebo_ros/node.hpp>
#include <rclcpp/rclcpp.hpp>
#include <sensor_msgs/msg/joint_state.hpp>
#include <std_msgs/msg/float64_multi_array.hpp>

#include <memory>
#include <string>
#include <vector>
#include <iostream>

namespace gazebo_plugins
{
class GazeboRosJointEffortSubscriberPrivate
{
public:
  /// Callback to be called at every simulation iteration.
  /// \param[in] info Updated simulation info.
  void OnUpdate(const gazebo::common::UpdateInfo& info);

  /// \brief Callback for set joint effort topic
  /// \param[in] msg joint efforts as Double Array
  void SetJointsEffort(std_msgs::msg::Float64MultiArray::SharedPtr _msg);

  /// A pointer to the GazeboROS node.
  gazebo_ros::Node::SharedPtr ros_node_;

  /// Joint state publisher.
  rclcpp::Subscription<std_msgs::msg::Float64MultiArray>::SharedPtr joint_effort_sub_;

  /// Joints being tracked.
  std::vector<gazebo::physics::JointPtr> joints_;

  /// Period in seconds
  double update_period_;

  std::vector<double> efforts_;

  /// Keep last time an update was published
  gazebo::common::Time last_update_time_;

  /// Pointer to the update event connection.
  gazebo::event::ConnectionPtr update_connection_;
};

GazeboRosJointEffortSubscriber::GazeboRosJointEffortSubscriber()
: impl_(std::make_unique<GazeboRosJointEffortSubscriberPrivate>())
{
}

void GazeboRosJointEffortSubscriber::Load(gazebo::physics::ModelPtr model, sdf::ElementPtr sdf)
{
  // ROS node
  impl_->ros_node_ = gazebo_ros::Node::Get(sdf);

  RCLCPP_INFO(impl_->ros_node_->get_logger(), "Gazebo loaded plubin Joint Effort Subscriber");

  // Update rate
  double update_rate = 1000.0;
  if (!sdf->HasElement("update_rate")) {
    RCLCPP_INFO(impl_->ros_node_->get_logger(), "Missing <update_rate>, defaults to %f",
      update_rate);
  } else {
    update_rate = sdf->GetElement("update_rate")->Get<double>();
  }

  if (update_rate > 0.0) {
    impl_->update_period_ = 1.0 / update_rate;
  } else {
    impl_->update_period_ = 0.0;
  }

  // Initialize joints, order matters of declaration is the order in the command vector
  if (!sdf->HasElement("joint_name")) {
    RCLCPP_ERROR(impl_->ros_node_->get_logger(), "Plugin missing <joint_name>s");
    impl_->ros_node_.reset();
    return;
  }

  sdf::ElementPtr joint_elem = sdf->GetElement("joint_name");
  while (joint_elem) {
    auto joint_name = joint_elem->Get<std::string>();

    auto joint = model->GetJoint(joint_name);
    if (!joint) {
      RCLCPP_ERROR(impl_->ros_node_->get_logger(), "Joint %s does not exist!", joint_name.c_str());
    } else {
      impl_->joints_.push_back(joint);
      RCLCPP_INFO(impl_->ros_node_->get_logger(), "Going to control joint [%s]",
        joint_name.c_str() );
        impl_->efforts_.push_back(0.0);
    }

    joint_elem = joint_elem->GetNextElement("joint_name");
  }

  if (impl_->joints_.empty()) {
    RCLCPP_ERROR(impl_->ros_node_->get_logger(), "No joints found.");
    impl_->ros_node_.reset();
    return;
  }

  impl_->last_update_time_ = model->GetWorld()->SimTime();

  impl_->joint_effort_sub_ = impl_->ros_node_->create_subscription<std_msgs::msg::Float64MultiArray>(
    "joint_effort_cmd", rclcpp::QoS(rclcpp::KeepLast(1)),
    std::bind(&GazeboRosJointEffortSubscriberPrivate::SetJointsEffort,
    impl_.get(), std::placeholders::_1));

  // Callback on every iteration
  impl_->update_connection_ = gazebo::event::Events::ConnectWorldUpdateBegin(
    std::bind(&GazeboRosJointEffortSubscriberPrivate::OnUpdate, impl_.get(), std::placeholders::_1));
}

void GazeboRosJointEffortSubscriberPrivate::OnUpdate(const gazebo::common::UpdateInfo & info)
{
  gazebo::common::Time current_time = info.simTime;

  // If the world is reset, for example
  if (current_time < last_update_time_) {
    RCLCPP_INFO(ros_node_->get_logger(), "Negative sim time difference detected.");
    last_update_time_ = current_time;
  }

  // Check period
  double seconds_since_last_update = (current_time - last_update_time_).Double();

  if (seconds_since_last_update < update_period_) {
    return;
  }

  // Do nothing until we receive messages.
  if (efforts_.empty())
  {
    return;
  }

  for (unsigned int i = 0; i < joints_.size(); ++i) {
    joints_[i]->SetForce(0, efforts_[i]);
  }

  // Update time
  last_update_time_ = current_time;
}


void GazeboRosJointEffortSubscriberPrivate::SetJointsEffort(std_msgs::msg::Float64MultiArray::SharedPtr _msg)
{

  std::size_t const effortsSize = efforts_.size();
  if (effortsSize != _msg->data.size()) {
    RCLCPP_ERROR(ros_node_->get_logger(),
      "Incompatible joint size received, expected %d, got %d", effortsSize, _msg->data.size());
    return;
  }

  efforts_ = _msg->data;
}

GZ_REGISTER_MODEL_PLUGIN(GazeboRosJointEffortSubscriber)
}  // namespace gazebo_plugins