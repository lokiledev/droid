#ifndef GAZEBO_PLUGINS__GAZEBO_ROS_JOINT_EFFORT_SUBSCRIBER_HPP_
#define GAZEBO_PLUGINS__GAZEBO_ROS_JOINT_EFFORT_SUBSCRIBER_HPP_

#include <gazebo/common/Plugin.hh>

#include <memory>

namespace gazebo_plugins
{
class GazeboRosJointEffortSubscriberPrivate;

/// Subscribe to joint effort commands from a given ROS topic and apply them in simulation.
/**
  \details The number and order of joint commands should match the one used in the simulation
  Example Usage:
  \code{.xml}
    <plugin name="gazebo_ros_joint_effort_subscriber"
        filename="libgazebo_ros_joint_effort_subscriber.so">
      <ros>
        <!-- Add a namespace -->
        <namespace>/new_namespace</namespace>
        <!-- Remap the default topic -->
        <remapping>joint_effort:=my_joint_effort</remapping>
      </ros>
      <!-- Update rate in Hertz -->
      <update_rate>2</update_rate>
    </plugin>
  \endcode
*/
class GazeboRosJointEffortSubscriber : public gazebo::ModelPlugin
{
public:
  /// Constructor
  GazeboRosJointEffortSubscriber();

  /// Destructor
  ~GazeboRosJointEffortSubscriber() = default;

protected:
  // Documentation inherited
  void Load(gazebo::physics::ModelPtr model, sdf::ElementPtr sdf) override;

private:
  /// Callback to be called at every simulation iteration.
  /// Private data pointer
  std::unique_ptr<GazeboRosJointEffortSubscriberPrivate> impl_;
};
}  // namespace gazebo_plugins
#endif  // GAZEBO_PLUGINS__GAZEBO_ROS_JOINT_EFFORT_SUBSCRIBER_HPP_