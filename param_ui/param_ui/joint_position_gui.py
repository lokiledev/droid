import sys
from argparse import ArgumentParser

import rclpy
from rclpy.node import Node
from trajectory_msgs.msg import JointTrajectoryPoint
from sensor_msgs.msg import JointState

from PySide2.QtWidgets import QApplication, QMainWindow, QSlider
from PySide2.QtCore import Slot, Qt, QEvent

DESCRIPTION="""A tool for sending joint positions commands in real time.
"""

class JointPositionWidget(QMainWindow):
    """
    @brief A widget for tuning a ros2 node parameters.
    @details Automatically fetch parameters at creation
             and send them when modified by user.
             For now only FLOAT parameters are supported.
    """
    def __init__(self, topic_name: str, low : float, high: float):
        super().__init__()
        self.node = Node('JointPositionControlInterface')
        self.topic = topic_name
        self.low = low
        self.high = high
        self.is_ready = False
        self.publisher = self.node.create_publisher(JointTrajectoryPoint, self.topic, 1)
        #self.pos_subcriber = self.node.create_subscription(JointState, '/joint_states', self.update_position, 1)
        self.slider = QSlider(Qt.Horizontal)
        self.slider.setMinimum(0)
        self.slider.setMaximum(1000)
        self.slider.setTickInterval(1)
        self.slider.setValue(500)
        self.slider.sliderMoved.connect(self.slider_changed)
        self.setCentralWidget(self.slider)

    def update_position(self, positions_msg : JointState):
        print(f"Received msg: {positions_msg.positions}")
        if not self.is_ready:
            start = positions_msg.positions[0]
            val = start * 1000 / (self.high -self.low)
            self.slider.setValue(val)
            print(f"Setting initial value to {start} rad == {val} ticks")
            self.is_ready = True

    @Slot(int)
    def slider_changed(self, value : int):
        msg = JointTrajectoryPoint()
        #msg.header.stamp = super().get_clock().now().to_msg()
        pos = self.low + value * (self.high - self.low) / 1000.0
        msg.positions = [pos]
        self.publisher.publish(msg)

    def keyPressEvent(self, event):
        key = event.key()
        if key == Qt.Key_Escape:
            self.close()


def main():
    parser = ArgumentParser(description=DESCRIPTION)
    parser.add_argument('topic', type=str, help="ROS2 topic to publish to")
    parser.add_argument('low', type=float, help="low range")
    parser.add_argument('high', type=float, help="high range")
    args = parser.parse_args()
    rclpy.init()

    # Create the Qt Application
    app = QApplication(sys.argv)
    w = JointPositionWidget(args.topic, args.low, args.high)
    w.show()
    w.setWindowTitle("Joint Position Control Interface")
    status = app.exec_()
    rclpy.shutdown()
    sys.exit(status)

if __name__ == '__main__':
    main()
