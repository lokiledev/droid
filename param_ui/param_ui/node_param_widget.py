import sys
import typing as tp
import rclpy

from PySide2.QtGui import QFont, QDoubleValidator
from PySide2.QtWidgets import (
    QApplication,
    QMainWindow,
    QLabel,
    QWidget,
    QGridLayout,
    QGroupBox,
    QCheckBox,
    QSpinBox,
    QDoubleSpinBox,
    QLineEdit
)

from PySide2.QtCore import Slot, Qt, QEvent

from .parameter_client import ParameterClient

def visit(widget : QWidget) -> tp.Any:
    """
    @brief Extract the value stored in a widget
    """
    if isinstance(widget, QCheckBox):
        return widget.isChecked()
    if isinstance(widget, QSpinBox):
        return widget.value()
    if isinstance(widget, QDoubleSpinBox):
        return widget.value()
    if isinstance(widget, QLineEdit):
        return str(widget.text())

class NodeParamWidget(QGroupBox):
    """
    @brief A widget for tuning a ros2 node parameters.
    @details Automatically fetch parameters at creation
             and send them when modified by user.
             For now only FLOAT parameters are supported.
    """
    def __init__(self, node_name =''):
        super().__init__(node_name)
        self.node = node_name
        self.setAlignment(Qt.AlignCenter)
        layout = QGridLayout()

        # Map of parameter names and widgets that can edit the corresponding type.
        self.widgets = {}
        self.client = ParameterClient(self.node)
        params = self.client.fetch_params()
        i = 0
        for param, value in params.items():
            label = QLabel(param)
            widget = self.create_widget(value)
            if widget is None:
                continue
            widget.installEventFilter(self)
            self.widgets[param] = widget
            layout.addWidget(label, i, 0)
            layout.addWidget(widget, i, 1)
            i += 1
        self.setLayout(layout)

    def eventFilter(self, obj, event):
        # Custom event filter to catch the enter key press.
        if event.type() == QEvent.KeyPress:
            if event.key() == Qt.Key_Return:
                self.param_changed()
                return True
        return False

    def create_widget(self, value : tp.Any) -> QWidget:
        """
        @brief Create the corresponding widget depending on input type.
               Ie : QLineEdit for a string, QDoubleSpinBox for a float etc.
        """
        widget = None
        if isinstance(value, bool):
            widget = QCheckBox()
            widget.setChecked(value)

        elif isinstance(value, int):
            widget = QSpinBox()
            widget.setMinimum(-10000)
            widget.setMaximum(10000)
            widget.setValue(value)

        elif isinstance(value, float):
            widget = QDoubleSpinBox()
            # TODO: read range and step from parameter description.
            widget.setSingleStep(0.01)
            widget.setMinimum(-10000.0)
            widget.setMaximum(10000.0)
            widget.setValue(value)

        elif isinstance(value, str):
            widget = QLineEdit()
            widget.setText(value)
        return widget


    def keyPressEvent(self, event):
        key = event.key()
        if key == Qt.Key_Escape:
            self.close()

    @Slot()
    def param_changed(self):
        """
        @brief Read values from all widgets and send the parameters only if they have changed
               since last call.
        """
        params = [ (key, visit(widget)) for key,widget in self.widgets.items()]
        self.client.send_params(params)
