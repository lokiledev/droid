import typing as tp
import rclpy
from rclpy.node import Node

from rclpy.parameter import Parameter, ParameterMsg
from rcl_interfaces.srv import ListParameters, GetParameters, SetParameters

class ParameterClient(Node):

    def __init__(self, target_node : str):
        super().__init__('ParameterClient')
        self.target_node = target_node
        # self.client_set = self.create_client(SetParameters, f"{target_node}/set_parameters")
        # self.client_set.wait_for_service()

    def call_get_parameters(self, parameter_names : tp.List[str]):
        """
        @see ros2param cli.
        """
        # create client
        client = self.create_client(GetParameters, f'{self.target_node}/get_parameters')

        # call as soon as ready
        ready = client.wait_for_service(timeout_sec=5.0)
        if not ready:
            raise RuntimeError('Wait for service timed out')

        request = GetParameters.Request()
        request.names = parameter_names
        future = client.call_async(request)
        rclpy.spin_until_future_complete(self, future)

        # handle response
        response = future.result()
        if response is None:
            e = future.exception()
            raise RuntimeError(
                f"Exception while calling service of node '{self.target_node}': {e}")
        return response

    def call_get_parameters_list(self):
        # create client
        client = self.create_client(ListParameters, f'{self.target_node}/list_parameters')

        # call as soon as ready
        ready = client.wait_for_service(timeout_sec=5.0)
        if not ready:
            raise RuntimeError('Wait for service timed out')

        request = ListParameters.Request()
        future = client.call_async(request)
        rclpy.spin_until_future_complete(self, future)

        # handle response
        response = future.result()
        if response is None:
            e = future.exception()
            raise RuntimeError(
                f"Exception while calling service of node '{self.target_node}': {e}")
        return response

    def call_set_parameters(self, parameters):
        """
        @see ros2param cli
        """
        # create client
        client = self.create_client(SetParameters, f'{self.target_node}/set_parameters')

        # call as soon as ready
        ready = client.wait_for_service(timeout_sec=5.0)
        if not ready:
            raise RuntimeError('Wait for service timed out')

        request = SetParameters.Request()
        request.parameters = parameters
        future = client.call_async(request)
        rclpy.spin_until_future_complete(self, future)

        # handle response
        response = future.result()
        if response is None:
            e = future.exception()
            raise RuntimeError(
                f"Exception while calling service of node '{self.target_node}': {e}")
        return response

    def fetch_params(self) -> tp.List[Parameter]:
        # First get parameter names.
        names = self.call_get_parameters_list().result.names
        # Then get the values for these names.
        # It doesn't return a Parameter message but a ParameterValue
        # So we need to rebuild a Parameter from these separated informations.
        values = self.call_get_parameters(names).values
        params = [ ParameterMsg(name=name, value=value) for name, value in zip(names, values)]
        result = {}
        for param_msg in params:
            param = Parameter.from_parameter_msg(param_msg)
            result[param.name] = param.value
        return result

    def send_params(self, params : tp.List[tp.Tuple[str, float]]):
        params_to_send = []
        for name, value in params:
            parameter = Parameter(name, Parameter.Type.from_parameter_value(value), value)
            params_to_send.append(parameter.to_parameter_msg())
        self.call_set_parameters(params_to_send)
