import sys
import rclpy
from argparse import ArgumentParser

from PySide2.QtWidgets import QApplication
from .node_param_widget import NodeParamWidget

DESCRIPTION="""A tool for changing in real time the parameters.
Values are populated only at startup, so if external sources
also update the parameters of the target node, it won't be
reflected here.
"""

def main():
    parser = ArgumentParser(description=DESCRIPTION)
    parser.add_argument('node', type=str, help="ROS2 node to configure.")
    args = parser.parse_args()
    node = args.node
    rclpy.init()
    # Create the Qt Application
    app = QApplication(sys.argv)
    w = NodeParamWidget(node)
    w.show()
    w.setWindowTitle("Node parameter tool")
    status = app.exec_()
    rclpy.shutdown()
    sys.exit(status)


if __name__ == '__main__':
    main()
