from setuptools import setup

package_name = 'param_ui'

setup(
    name=package_name,
    version='0.1.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='loki',
    maintainer_email='contact@lokile.dev',
    description='A tool for easily tune ros2 node parameters at runtime. One could say it replaces rqt-reconfigure',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'ui = param_ui.ui:main',
            'jpc = param_ui.joint_position_gui:main'
        ],
    },
)
