import typing as tp
from math import radians

from .motor import MotorConfig
from .transmission import TransmissionConfig

FRONTAL_HIP_MOTOR = MotorConfig(
    kv = 240.0,
    max_current = 10.0,
    position_gain = 150.0,
    velocity_gain = 0.0001,
    can_id = 1,
    direction = 1.0,
    name = "frontal_hip"
)

SAGITTAL_HIP_MOTOR = MotorConfig(
    kv = 240.0,
    max_current = 10.0,
    position_gain = 150.0,
    velocity_gain = 0.0001,
    can_id = 2,
    direction = -1.0,
    name = "sagittal_hip"
)

KNEE_MOTOR = MotorConfig(
    kv = 240.0,
    max_current = 10.0,
    position_gain = 150.0,
    velocity_gain = 0.0001,
    can_id = 3,
    direction = 1.0,
    name = "knee"
)

FRONTAL_HIP = TransmissionConfig(
    ratio = 8.0,
    zero = 0.0, # read it init for now.
    low = radians(-60.0),
    high = radians(60.0)
)

SAGITTAL_HIP = TransmissionConfig(
    ratio = 8.0,
    zero = 0.0, # read it init for now.
    low = radians(-90.0),
    high = radians(110.0)
)

SAGITTAL_KNEE = TransmissionConfig(
    ratio = 8.0,
    zero = 0.0, # read it init for now.
    low = radians(-60.0),
    high = radians(150.0)
)
