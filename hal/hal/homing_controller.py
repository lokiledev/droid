from math import radians
from enum import Enum

class HomingState(Enum):
    INIT = 0
    HIGH_STOP = 1
    WAITING = 2
    ZERO = 3
    READY = 4

class HomingController:
    """
    @class HomingController: A controller to home an axis.
    """
    def __init__(self, name: str, start_pos : float, end_pos: float = 0.0):
        self.name = name
        self.end_pos = end_pos
        self.current_pos = start_pos
        self.target_pos = start_pos
        self.state : HomingState = HomingState.INIT
        self.velocity = radians(400.0)
        self.contact_threshold = radians(30.0)
        self.endstop_angle = self.current_pos
        self.final_destination = self.current_pos

    def get_state(self) -> HomingState:
        return self.state

    def start(self):
        if self.state == HomingState.INIT:
            print(f"{self.name} starting!")
            self.state = HomingState.HIGH_STOP
        if self.state == HomingState.WAITING:
            print(f"{self.name} stop waiting, going to ZERO")
            self.state = HomingState.ZERO

    def update(self, current_pos : float, period : float) -> float:
        """
        @brief compute a new target based on current position.
        """
        self.current_pos = current_pos
        if self.state == HomingState.INIT:
            pass

        if self.state == HomingState.HIGH_STOP:
            self.target_pos = self.target_pos + self.velocity * period
            if abs(self.target_pos - self.current_pos) > self.contact_threshold:
                print(f"{self.name} endstop reached, now waiting")
                self.target_pos = self.current_pos
                self.endstop_angle = self.current_pos
                self.final_destination = self.endstop_angle - self.end_pos
                self.state = HomingState.WAITING

        if self.state == HomingState.WAITING:
            self.target_pos = self.current_pos

        if self.state == HomingState.ZERO:
            sign = 1.0
            if self.target_pos - self.final_destination < 0.0:
                sign = -1.0
            self.target_pos = self.target_pos - sign*self.velocity * period
            if abs(self.final_destination - self.target_pos) <= radians(2.0):
                print(f"{self.name} zero reached, now READY!")
                self.state = HomingState.READY
                self.target_pos = self.final_destination

        if self.state == HomingState.READY:
            self.target_pos = self.current_pos

        return self.target_pos

    def get_endstop(self) -> float:
        return self.endstop_angle
