import typing as tp
from math import radians, degrees
import rclpy
import time

from rclpy.node import Node
from rclpy.parameter import Parameter

from std_msgs.msg import Float64MultiArray
from trajectory_msgs.msg import JointTrajectoryPoint

from sensor_msgs.msg import JointState

from rclpy.exceptions import ParameterNotDeclaredException
from rcl_interfaces.msg import ParameterType, SetParametersResult

from copy import copy

from .transmission import Transmission, Kinematic, TransmissionConfig
from .motor import Motor, get_bus
from .hal_config import FRONTAL_HIP_MOTOR, SAGITTAL_HIP_MOTOR, KNEE_MOTOR
from .hal_config import FRONTAL_HIP, SAGITTAL_HIP, SAGITTAL_KNEE
from .homing_controller import HomingController, HomingState


JOINTS_NAMES = ['frontal_hip', 'sagittal_hip', 'knee']
class HardwareAbstractionLayer(Node):

    def __init__(self, period : float = 0.01):
        super().__init__('HardwareAbstractionLayer')
        self.is_calibrating = False
        self.controllers = []
        self.motors = []
        self.motors_target = []
        self.transmissions = []
        self.joints_targets = []
        self.init_drivers()
        self.init_transmissions()
        self.joint_publisher_ = self.create_publisher(JointState, 'joint_states', 1)
        self.motor_publisher_ = self.create_publisher(JointState, 'motor_states', 1)
        self.motor_target_publisher_ = self.create_publisher(JointState, 'motor_targets', 1)
        self.subscription = self.create_subscription(
            JointTrajectoryPoint,
            'joint_position_cmd',
            self.update_position_cmd,
            1)

        self.init_parameters()
        self.init_targets()
        self.period = period
        self.timer = self.create_timer(self.period, self.routine)
        self.get_logger().info(f"Hardware Abstraction Layer is ready, period: {period} sec")
        self.motors[0].start()
        self.motors[1].start()
        self.motors[2].start()
        self.start_calibration()

    def start_calibration(self):
        self.is_calibrating = True
        for motor, transmission in zip(self.motors, self.transmissions):
            pos, _ = motor.state()
            controller = HomingController(motor.name, pos, transmission.high * transmission.ratio)
            self.controllers.append(controller)
        self.controllers[0].start()

    def init_parameters(self):
        """
        init ros parameters
        """
        # Use first motor as init values, they are applied to all of them.
        position_gain = self.motors[0].position_gain
        # Scale velocity param because it's too small
        velocity_gain = self.motors[0].velocity_gain * 1000.0
        integrator_gain = self.motors[0].integrator_gain * 1000.0
        self.declare_parameter('position_gain', position_gain)
        self.declare_parameter('velocity_gain', velocity_gain)
        self.declare_parameter('integrator_gain', integrator_gain)
        self.add_on_set_parameters_callback(self.update_parameters)

    def update_parameters(self, params : tp.List[Parameter]):
        pos_gain = self.motors[0].position_gain
        vel_gain = self.motors[0].velocity_gain
        integrator_gain = self.motor.integrator_gain
        for param in params:
            if param.name == 'position_gain':
                pos_gain = param.value
            if param.name == 'velocity_gain':
                vel_gain = param.value / 1000.0
            if param.name == 'integrator_gain':
                integrator_gain = param.value / 1000.0
        for motor in self.motors:
            motor.set_gains(pos_gain, vel_gain, integrator_gain)
        return SetParametersResult(successful=True)

    def init_drivers(self):
        bus = get_bus()
        self.motors.append(Motor(FRONTAL_HIP_MOTOR, bus))
        self.motors.append(Motor(SAGITTAL_HIP_MOTOR, bus))
        self.motors.append(Motor(KNEE_MOTOR, bus))
        self.get_logger().info(f'All motors initialized')

    def init_transmissions(self):
        transmission_conf = [FRONTAL_HIP, SAGITTAL_HIP, SAGITTAL_KNEE]
        for i, motor in enumerate(self.motors):
            conf = copy(transmission_conf[i])
            # Read initial position and use it as the transmission zero.
            conf.zero = motor.position()
            self.transmissions.append(Transmission(conf))

    def init_targets(self) -> None:
        """
        @brief Initialize target joint position to initial position (stay in place)
        """
        self.joints_target = []
        self.motors_target = []
        for motor in self.motors:
            pos, vel = motor.state()
            motor_state = Kinematic(pos, vel)
            self.motors_target.append(motor_state)
        self.joints_target = self.actuator_to_joint(self.motors_target)

    def update_torque_cmd(self, msg : Float64MultiArray):
        self.torque_target = msg.data[0]

    def update_position_cmd(self, msg : JointTrajectoryPoint):
        for i, pos in enumerate(msg.positions):
            self.joints_target[i] = Kinematic(pos, 0.0)

    def send_targets(self, motor_targets: tp.List[Kinematic]):
        motor_msg = JointState()
        motor_msg.header.stamp = super().get_clock().now().to_msg()

        for i, target in enumerate(motor_targets):
            motor_msg.name.append(self.motors[i].name)
            motor_msg.position.append(motor_targets[i].position)
            motor_msg.velocity.append(motor_targets[i].velocity)
            self.motors[i].set_kinematic_target(target.position, target.velocity)
        self.motor_target_publisher_.publish(motor_msg)

    def actuator_to_joint(self, motor_positions: tp.List[Kinematic]) -> tp.List[Kinematic]:
        actuators_state = []
        for motor_state, transmission in zip(motor_positions, self.transmissions):
            joint_state = transmission.actuator_to_joint(motor_state)
            actuators_state.append(joint_state)

        joint_states = []
        joint_states.append(actuators_state[0])
        joint_states.append(actuators_state[1])

        # Compute real knee angle from knee and hip actuator
        hip_pos = actuators_state[1].position
        knee_actuator = actuators_state[2].position
        knee_state = radians(180) - knee_actuator + hip_pos
        joint_states.append(Kinematic(position=knee_state))
        return joint_states

    def joint_to_actuator(self, joint_positions: tp.List[Kinematic]) -> tp.List[float]:
        hip_pos = joint_positions[1].position
        knee_pos = joint_positions[2].position
        knee_actuator_pos = radians(180) - knee_pos + hip_pos
        targets = [joint_positions[0], joint_positions[1], Kinematic(knee_actuator_pos)]

        motor_targets = []
        for transmission, joint in zip(self.transmissions, targets):
            target = transmission.joint_to_actuator(joint)
            motor_targets.append(target)
        return motor_targets

    def read_state(self) -> tp.List[Kinematic]:
        motors_state = []
        motor_msg = JointState()
        motor_msg.header.stamp = super().get_clock().now().to_msg()
        for motor in self.motors:
            pos, vel = motor.state()
            motor_state = Kinematic(pos, vel)
            motors_state.append(motor_state)
            motor_msg.name.append(motor.name)
            motor_msg.position.append(motor_state.position)
            motor_msg.velocity.append(motor_state.velocity)
        self.motor_publisher_.publish(motor_msg)
        return motors_state

    def publish_joints_state(self, joints_state: tp.List[Kinematic]):
        joint_msg = JointState()
        joint_msg.header.stamp = super().get_clock().now().to_msg()
        for i, joint in enumerate(joints_state):
            joint_msg.name.append(JOINTS_NAMES[i])
            joint_msg.position.append(joint.position)
            joint_msg.velocity.append(joint.velocity)
        self.joint_publisher_.publish(joint_msg)

    def calibrate(self) -> bool:
        targets = []

        states = [c.get_state() for c in self.controllers]

        # First axis constroller
        if states[0] == HomingState.READY:
            if states[1] == HomingState.INIT and states[2] == HomingState.INIT:
                self.controllers[1].start()
                self.controllers[2].start()

        if states[0] == HomingState.WAITING:
            self.controllers[0].start()

        if states[1] == HomingState.WAITING and states[2] == HomingState.WAITING:
            self.controllers[1].start()
            self.controllers[2].start()

        for state, controller in zip(self.motors_state, self.controllers):
            target_pos = controller.update(state.position, self.period)
            targets.append(Kinematic(target_pos))

        self.motors_target = targets
        return all([ c.get_state() == HomingState.READY for c in self.controllers])

    def routine(self):
        self.motors_state = self.read_state()
        self.joint_states = self.actuator_to_joint(self.motors_state)
        self.publish_joints_state(self.joint_states)
        if not self.is_calibrating:
            self.motors_target = self.joint_to_actuator(self.joints_target)
        else:
            finished = self.calibrate()
            if finished:
                for controller, transmission in zip(self.controllers, self.transmissions):
                    transmission.compute_zero(high=controller.get_endstop())
                self.joint_targets = self.actuator_to_joint(self.motors_state)
                self.get_logger().info(f'Robot is calibrated!  \o/')
                # self.motors[0].stop()
                # self.motors[1].stop()
                # self.motors[2].stop()
            self.is_calibrating = not finished
        self.send_targets(self.motors_target)

    def stop(self):
        self.get_logger().info(f'Stopping HAL Node')
        for motor in self.motors:
            motor.tm.idle()

def main(args=None):
    rclpy.init(args=args)
    hal_node = HardwareAbstractionLayer()
    try:
        rclpy.spin(hal_node)
    except KeyboardInterrupt:
        pass
    hal_node.stop()
    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    hal_node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
