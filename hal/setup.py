from setuptools import setup

package_name = 'hal'

setup(
    name=package_name,
    version='0.1.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools', 'tinymovr'],
    zip_safe=True,
    maintainer='Loki Le Dev',
    maintainer_email='contact@lokile.dev',
    description="""Hardware Abstraction layer for project DROID. Implement low level, vendor specific hardware drivers and provide a standard ros messages interface""",
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'droid_hal = hal.droid_hal:main'
        ],
    },
)
