Use a virtualenv for installing dependencies!
Actually there is a tutorial: https://index.ros.org/doc/ros2/Tutorials/Using-Python-Packages/

**Note**: Well I can't find a way to make the virtualenv work.
The tinymovr package is correctly installed I can use it
inside the workspace when I run `python -c "import tinymovr"`

But running the ros2 node that imports it doesn't work.
So I am forced to installed in a system wide fashion.

# Installing dependencies to package the project

**colcon** is only a build tool.
**rosdep** is the tool that manages dependencies.

**colcon** doesn't install external python dependencies written
in the **setup.py** file, in the `install_requires` field.

In order to install a python dependency, you must declare it
in the **package.xml** file to tell **rosdep** about it.

And then in order for **rosdep** to find it, it must exist in
the [rosdep index](https://github.com/ros/rosdistro/blob/master/rosdep/python.yaml).

If you don't want to submit a change to this file,
you can add a local index file.
Following [this tutorial](https://docs.ros.org/en/independent/api/rosdep/html/sources_list.html)

I created the simple **hal-python.yaml** file, let's update
the installed config of rosdep so that we can finally install simpe pip package.

This is not ideal because it installs some config file to a system folder.

```sh
sudo mkdir -p /usr/local/share/ros && sudo cp hal-python.yaml /usr/local/share/ros
echo "yaml file:///usr/local/share/ros/hal-python.yaml" | sudo tee -a /etc/ros/rosdep/sources.list.d/20-default.list
rosdep update
```

**NOTE** The real solution is to submit a pr to update the rosdep key
when the package should be released via the bloom tool.
