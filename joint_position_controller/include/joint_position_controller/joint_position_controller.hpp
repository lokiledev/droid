#ifndef JOINT_POSITION_CONTROLLER_H
#define JOINT_POSITION_CONTROLLER_H

#include <vector>
#include <unordered_map>
#include <string>

#include <pid/pid.hpp>

class JointPositionController
{
public:
    JointPositionController() = default;
    JointPositionController(std::vector<std::string> const& jointNames);

    void setTargets(std::vector<double> const& targets);
    std::vector<double> update(std::vector<double> const& inputs, double const time);
    void setParam(std::string const& joint, std::string const& param, double const value);
    void printParams();
    std::size_t nbJoints() { return joints_.size();};
private:
    std::vector<Pid> controllers_{};
    std::unordered_map<std::string, std::size_t> joints_;
};

#endif