#ifndef JOINT_POSITION_CONTROLLER_NODE_H
#define JOINT_POSITION_CONTROLLER_NODE_H

#include <vector>
#include <unordered_map>
#include <string>

#include <rclcpp/rclcpp.hpp>
#include <std_msgs/msg/float64_multi_array.hpp>
#include <sensor_msgs/msg/joint_state.hpp>

#include "joint_position_controller/joint_position_controller.hpp"

class JointPositionControllerNode : public rclcpp::Node
{
public:
    JointPositionControllerNode();
    void init();
    void load_config();
    void on_timer();
    void on_targets(std_msgs::msg::Float64MultiArray::SharedPtr targets);
    void on_joint_states(sensor_msgs::msg::JointState::SharedPtr joints);
    rcl_interfaces::msg::SetParametersResult on_param_changed(const std::vector<rclcpp::Parameter>& parameters);
private:
    void set_pid_param(rclcpp::Parameter const& parameter);

    rclcpp::Publisher<std_msgs::msg::Float64MultiArray>::SharedPtr publisher_;
    rclcpp::Subscription<std_msgs::msg::Float64MultiArray>::SharedPtr targetSub_;
    rclcpp::Subscription<sensor_msgs::msg::JointState>::SharedPtr measurementSub_;
    OnSetParametersCallbackHandle::SharedPtr parametersCb_;
    rclcpp::TimerBase::SharedPtr timer_;
    std::vector<double> inputs_;
    JointPositionController controller_{};
    rclcpp::Time lastTime_;
    bool isReady_{false};
};

#endif