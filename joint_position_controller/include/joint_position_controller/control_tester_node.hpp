#ifndef CONTROL_TESTER_NODE_H
#define CONTROL_TESTER_NODE_H

#include <vector>
#include <unordered_map>
#include <string>

#include <rclcpp/rclcpp.hpp>
#include <trajectory_msgs/msg/joint_trajectory_point.hpp>
#include <sensor_msgs/msg/joint_state.hpp>


class Wave;

/// \class ControlTesterNode A node for sending specific
///        targets to a controller for tuning.
class ControlTesterNode : public rclcpp::Node
{
public:
    ControlTesterNode();
    void on_timer();
    rcl_interfaces::msg::SetParametersResult on_param_changed(const std::vector<rclcpp::Parameter>& parameters);
private:
    void declare_wave_parameters();
    void apply_wave_parameters();

    void switch_targets();
    void on_joint_states(sensor_msgs::msg::JointState::SharedPtr joints);

    rclcpp::Publisher<trajectory_msgs::msg::JointTrajectoryPoint>::SharedPtr publisher_;
    rclcpp::Subscription<sensor_msgs::msg::JointState>::SharedPtr jointStatesSub_;
    rclcpp::TimerBase::SharedPtr timer_;
    OnSetParametersCallbackHandle::SharedPtr parametersCb_;

    std::vector<double> targetPositions_{};
    std::vector<double> targetVelocities_{};

    std::vector<double> positions_;
    std::shared_ptr<Wave> wave_{};
    bool param_changed_{true};
    bool target_matched_{false};

    double const CATCHUP{0.001}; ///< Position increment for catching new target
};

#endif