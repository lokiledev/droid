#include <joint_position_controller/joint_position_controller_node.hpp>

#include <chrono>
#include <functional>
#include <memory>
#include <string>
#include <assert.h>

using namespace std::chrono_literals;
using namespace std::chrono;
using seconds_f = duration<double>;

JointPositionControllerNode::JointPositionControllerNode()
    : Node("joint_position_controller_node")
{
    declare_parameter<std::vector<std::string>>("joints", {});
    load_config();
    init();
}

void JointPositionControllerNode::init()
{
    publisher_ = create_publisher<std_msgs::msg::Float64MultiArray>("joint_effort_cmd", 1);
    timer_ = create_wall_timer(5ms, std::bind(&JointPositionControllerNode::on_timer, this));

    targetSub_ = create_subscription<std_msgs::msg::Float64MultiArray>(
    "joint_position_cmd",
    1,
    std::bind(&JointPositionControllerNode::on_targets, this, std::placeholders::_1));

    measurementSub_ = create_subscription<sensor_msgs::msg::JointState>(
    "joint_states",
    1,
    std::bind(&JointPositionControllerNode::on_joint_states, this, std::placeholders::_1));

    parametersCb_ = this->add_on_set_parameters_callback(
        std::bind(&JointPositionControllerNode::on_param_changed, this, std::placeholders::_1));

    lastTime_ = now();

    RCLCPP_INFO(this->get_logger(), "Joint position controller is ready to go!");
}

rcl_interfaces::msg::SetParametersResult JointPositionControllerNode::on_param_changed(
    const std::vector<rclcpp::Parameter>& parameters)
{
    rcl_interfaces::msg::SetParametersResult result;
    result.successful = true;
    result.reason = "success";

    for (const auto& param : parameters)
    {
        if (param.get_name() != "joints" and param.get_name() != "use_sim_time")
        {
            set_pid_param(param);
        }
    }

    return result;
}

void JointPositionControllerNode::set_pid_param(rclcpp::Parameter const& parameter)
{
    std::string full_name = parameter.get_name();
    auto pos = full_name.find('.');
    std::string const joint = full_name.substr(0, pos);
    std::string const pid_value = full_name.substr(pos+1);

    double value = parameter.get_value<double>();
    controller_.setParam(joint, pid_value, value);
    RCLCPP_DEBUG(this->get_logger(), "Changing %s of %s to %f", pid_value.c_str(), joint.c_str(), value);
}

void JointPositionControllerNode::load_config()
{
    std::vector<std::string> joints = this->get_parameter("joints").get_value<std::vector<std::string>>();
    controller_ = JointPositionController(joints);
    RCLCPP_INFO(this->get_logger(), "Creating controller for %d joints", joints.size());

    for (auto joint : joints)
    {
        std::map<std::string, double> pid_params{
            {"kp", 0},
            {"ki", 0},
            {"kd", 0},
            {"min", 0},
            {"max", 0},
            {"min_integral", -1000.0},
            {"max_integral", 1000.0},
            };

        this->declare_parameters(joint, pid_params);
        std::map<std::string, double> set_params;
        this->get_parameters(joint, set_params);
        for (auto param: set_params)
        {
            controller_.setParam(joint, param.first, param.second);
        }
    }

    // Print initial values.
    controller_.printParams();
}

void JointPositionControllerNode::on_timer()
{
    // Some circonvolutions to convert nanosecond int to seconds double.
    rclcpp::Time current_time = now();
    nanoseconds nanos((current_time - lastTime_).nanoseconds());
    double dt = duration_cast<seconds_f>(nanos).count();
    lastTime_ = current_time;
    if (isReady_) {
        std_msgs::msg::Float64MultiArray msg;
        msg.data = controller_.update(inputs_, dt);
        // Publish targets
        publisher_->publish(msg);
    }
}

void JointPositionControllerNode::on_joint_states(sensor_msgs::msg::JointState::SharedPtr joints)
{
    if (not isReady_) {
        // Use the first received measured position as an initial target
        // to stay in place.
        controller_.setTargets(joints->position);
        isReady_ = true;
        RCLCPP_INFO(this->get_logger(), "First positions received, taking control");
    }
    inputs_ = joints->position;
    RCLCPP_DEBUG(this->get_logger(), "received joint states");
}

void JointPositionControllerNode::on_targets(std_msgs::msg::Float64MultiArray::SharedPtr targets)
{
    controller_.setTargets(targets->data);
    RCLCPP_DEBUG(this->get_logger(), "received targets");
}