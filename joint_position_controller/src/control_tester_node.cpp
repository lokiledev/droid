#include <joint_position_controller/control_tester_node.hpp>

#include <chrono>
#include <functional>
#include <memory>
#include <string>
#include <assert.h>

#include <waveform/Utils.h>
#include <waveform/Square.h>
#include <waveform/Sinus.h>
#include <waveform/Triangle.h>
#include <waveform/Ramp.h>
#include <waveform/Constant.h>


using namespace std::chrono_literals;
using namespace std::chrono;

using secondsf = duration<double>;

ControlTesterNode::ControlTesterNode()
    : Node("control_tester")
    , targetPositions_{0.0}
    , targetVelocities_{0.0}
    , positions_{0.0}
{
    declare_wave_parameters();
    wave_ = std::make_shared<Constant>();

    publisher_ = create_publisher<trajectory_msgs::msg::JointTrajectoryPoint>("joint_position_cmd", 1);
    jointStatesSub_ = create_subscription<sensor_msgs::msg::JointState>(
    "joint_states",
    1,
    std::bind(&ControlTesterNode::on_joint_states, this, std::placeholders::_1));

    timer_ = create_wall_timer(5ms, std::bind(&ControlTesterNode::on_timer, this));

    parametersCb_ = this->add_on_set_parameters_callback(
        std::bind(&ControlTesterNode::on_param_changed, this, std::placeholders::_1));
}

void ControlTesterNode::declare_wave_parameters()
{
     std::map<std::string, double> wave_params{
        {"amplitude", 60.0},
        {"period_ms", 10000.0},
        {"offset", 0.0},
        };
    this->declare_parameters("waveform", wave_params);
    this->declare_parameter<std::string>("waveform.type", "constant");
    this->declare_parameter<bool>("waveform.enable", false);
}

void ControlTesterNode::apply_wave_parameters()
{
    if (param_changed_)
    {
        double amplitude{50.0};
        get_parameter("waveform.amplitude", amplitude);
        amplitude = deg2rad(amplitude);
        double period_ms{10000.0};
        get_parameter("waveform.period_ms", period_ms);
        double frequency = 1000.0 / period_ms;
        double offset{0.0};
        get_parameter("waveform.offset", offset);
        offset = deg2rad(offset);
        std::string type{"sinus"};
        get_parameter("waveform.type", type);

        if (type == "sinus")
        {
            wave_ = std::make_shared<Sinus>(frequency, amplitude, 0.0, offset);
        }
        else if (type == "square")
        {
            wave_ = std::make_shared<Square>(frequency, amplitude, offset);
        }
        else if (type == "triangle")
        {
            wave_ = std::make_shared<Triangle>(frequency, amplitude, offset);
        }
        else if (type == "ramp")
        {
            wave_ = std::make_shared<Ramp>(amplitude, offset);
        }
        else if (type == "constant")
        {
            wave_ = std::make_shared<Constant>(amplitude);
        }
        else
        {
            RCLCPP_WARN(get_logger(), "Type %s is not supported!", type.c_str());
            // TODO: Handle parameters in "on_param_changed" callback and respond failure in case of invalid type.
            return;
        }

        // Inject a time reference passed as a lambda.
        wave_->set_clock([this]() -> double {
            // Converting between ros time and std::chrono is tedious.
            // Try to get a double value of absolute elapsed seconds
            // Precision might be impaired but it should be enough.
            nanoseconds t(this->now().nanoseconds());
            return duration_cast<secondsf>(t).count();
            });

        bool enabled{false};
        get_parameter("waveform.enable", enabled);
        if (enabled)
        {
            wave_->start();
        }

        param_changed_ = false;
        RCLCPP_DEBUG(get_logger(), "Changed wave to: [%s] %s amplitude: %f, frequency: %f, offset: %f",
            enabled ? "ON" : "OFF", type.c_str(),
            amplitude, frequency, offset);
        target_matched_ = false;
        // Reset targets to current position
        // to avoid discontinuities
        targetPositions_[0] = positions_[0];
    }
}

void ControlTesterNode::on_joint_states(sensor_msgs::msg::JointState::SharedPtr joints)
{
    positions_ = joints->position;
}

void ControlTesterNode::on_timer()
{
    apply_wave_parameters();
    if (wave_->enabled())
    {
        switch_targets();
        trajectory_msgs::msg::JointTrajectoryPoint msg;
        msg.positions = targetPositions_;
        msg.velocities = targetVelocities_;
        publisher_->publish(msg);
    }
}

rcl_interfaces::msg::SetParametersResult ControlTesterNode::on_param_changed(
    const std::vector<rclcpp::Parameter>&)
{
    rcl_interfaces::msg::SetParametersResult result;
    result.successful = true;
    result.reason = "success";
    param_changed_ = true;
    return result;
}

 void ControlTesterNode::switch_targets()
 {
     Command command = wave_->update();
     double new_target = command.position;
     double target_vel = command.velocity;

     double target_pos = new_target;
     if (not target_matched_)
     {
        target_vel = 0.0; // Only change target position;
        if (targetPositions_[0] < target_pos)
        {
            target_pos = targetPositions_[0] + CATCHUP;
        }
        else
        {
            target_pos = targetPositions_[0] - CATCHUP;
        }

        if (abs(new_target - targetPositions_[0]) < 2.0 * CATCHUP)
        {
            target_matched_ = true;
        }
     }
     targetPositions_[0] = target_pos;
     targetVelocities_[0] = target_vel;
 }
