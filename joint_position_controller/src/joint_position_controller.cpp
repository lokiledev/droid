#include <joint_position_controller/joint_position_controller.hpp>

#include <iostream>

JointPositionController::JointPositionController(std::vector<std::string> const& jointNames)
{
    // Create a map for joint indexes for easy access later.
    for(std::size_t i=0; i < jointNames.size(); i++)
    {
        joints_[jointNames[i]] = i;
        controllers_.push_back(Pid());
    }
}

void JointPositionController::printParams()
{
    for (auto& pid : controllers_)
    {
        pid.print();
    }
}

void JointPositionController::setParam(std::string const& joint, std::string const& param, double const value)
{
    // Setting an invalid joint does nothing.
    if (joints_.find(joint) == joints_.end())
    {
        return;
    }

    // Quick and dirty way to map strings to parameters,
    // since there arent that many.
    Pid& controller = controllers_[joints_.at(joint)];
    if(param == "kp") {
        controller.setKp(value);
    } else if(param == "ki") {
        controller.setKi(value);
    } else if(param == "kd") {
        controller.setKd(value);
    } else if(param == "min") {
        controller.setMin(value);
    } else if(param == "max") {
        controller.setMax(value);
    } else if(param == "max_integral") {
        controller.setMaxIntegral(value);
    } else if(param == "min_integral") {
        controller.setMinIntegral(value);
    }
}

void JointPositionController::setTargets(std::vector<double> const& targets)
{ 
    for(std::size_t i = 0; i < controllers_.size(); i++)
    {
        controllers_[i].setTarget(targets[i]);
    }
}

std::vector<double> JointPositionController::update(std::vector<double> const& inputs, double const delta_time)
{
    std::vector<double> outputs(inputs.size());

    for (std::size_t i = 0; i < controllers_.size(); i++)
    {
        outputs[i] = controllers_[i].update(inputs[i], delta_time);
    }
    return outputs;
}