#include <memory>
#include <rclcpp/rclcpp.hpp>

#include <joint_position_controller/control_tester_node.hpp>

int main(int argc, char * argv[])
{
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<ControlTesterNode>());
    rclcpp::shutdown();
    return 0;
}