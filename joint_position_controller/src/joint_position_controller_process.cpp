#include <joint_position_controller/joint_position_controller.hpp>

#include <memory>
#include <rclcpp/rclcpp.hpp>

#include <joint_position_controller/joint_position_controller_node.hpp>

int main(int argc, char * argv[])
{
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<JointPositionControllerNode>());
    rclcpp::shutdown();
    return 0;
}