#include <pid/pid.hpp>
#include <pid/utils.hpp>

#include <iostream>

Pid::Pid(double const kp,
         double const ki,
         double const kd,
         double const minOutput,
         double const maxOutput,
         double const minIntegral,
         double const maxIntegral)
    :kp_{kp}
    ,ki_{ki}
    ,kd_{kd}
    ,minIntegral_{minIntegral}
    ,maxIntegral_{maxIntegral}
    ,min_{minOutput}
    ,max_{maxOutput}
{
}

void Pid::print()
{
    std::cout
        << "kp: " << kp_ << " ki: " << ki_ << " kd: " << kd_ << std::endl
        << "min: " << min_ << " max: " << max_ << " imin: "
        << minIntegral_ << " imax: " << maxIntegral_ << std::endl;
}

double Pid::update(double const input, double const delta_time)
{
    double const error = target_ - input;
    double const p_term = kp_ * error;

    double i_error = error * delta_time + error_integral_;
    double i_term = ki_ * i_error;
    // Be careful here of division by 0.
    if (ki_ != 0.0)
    {
        if (i_term > maxIntegral_)
        {
            i_term = maxIntegral_;
            i_error = error_integral_;
        }
        else if (i_term < minIntegral_)
        {
            i_term = minIntegral_;
            i_error = error_integral_;
        }
    }
    double const d_term = kd_ * ((error - previous_error_) / delta_time);
    double const output = p_term + i_term + d_term;

    output_ = clamp(output, min_, max_);
    previous_error_ = error;
    error_integral_ = i_error;
    return output_;
}

void Pid::reset()
{
    error_integral_ = 0.0;
    target_ = 0.0;
    output_ = 0.0;
    previous_error_ = 0.0;
}