# Welcome to PROJECT _DROID_

This is the source code for the project DROID (still a codename).

## About

Project DROID goal is to create a companion robot, like a pet robot, that can walk with me in the street.
I believe robotic technology is now at a level where we can build amazing things. Every brick is there somewhere,
I and want to combine them to bring this robot to life.

The project is documented on [Youtube](https://www.youtube.com/watch?v=lRAuK1sAMy4&list=PLZvbNAMaWQBl1rb4N9nYZDSVbkcV5tihL)
so feel free to follow and comment!

To make it easier to follow, I've create tags like `episode_007` so that you can checkout the code and see the version corresponding
to each video. I'll try to keep doig this but sometimes it might be hard to synchronize.

## The codebase

This repository contains the main codebase of the robot, for now I will try to put as much as possible into a single
repository, because it's easier to maintain and to centralize contributions.

The code is mainly C++ and python, and I'm using ROS2 foxy for the great tools and modular architecture.
There is a simulation based on gazebo 11.

## Installing and running the project

For now I can't spend much time on making tutorials and making sure everyone can easily build and run the project,
I'm still figuring out the proper dev environment for me.

## How to contribute

For now there aren't any open issues, I'll add some for project management purposes.
Feel free to create issues with the label `question` or `request`, I'll try to answer them.
If many people upvote the issue it will help me prioritize them. For example you can ask for a docker image,
or a vscode settings that work with the project etc.

## Wiki
I think it's great to create documentation even just for me, to keep track of the design decisions, dimmensionning,
choice of components etc. Feel free to add other pages in the wiki to help newcomers understand the project :)

** Let's BUILD!**


## Exporting urdf from onshape

**WARNING** Be careful when exporting from onshape, you must set all the revolute
joints to 0° their origin. Because it will export the parts "as is" and the stls
will not be orientated correctly else.

